<?php
	
/**
 * merge-sites.php
 * A PHP script to help merge a single ExpressionEngine install into a Multiple Site Manager setup.
 * WARNING: This is not a polished solution by any means, so be smart and use at your own risk.
 * I'm not responsible if you don't make backups and hose your ExpressionEngine installs.
 *
 * @author Matt Stein (matt@workingconcept.com)
 * @version 0.9a
 * @copyright January 6, 2012
 **/

/**
 * 1. Modify /system/expressionengine/core/EE_Config.php so we can see and test the current site after DB updates.
 */

// A. comment out lines 168-172:
	// if ( ! file_exists(APPPATH.'libraries/Sites.php') OR ! isset($this->default_ini['multiple_sites_enabled']) OR $this->default_ini['multiple_sites_enabled'] != 'y')
	// {
	// 	$site_name = '';
	// 	$site_id = 1;
	// }	
// B. add "$query = $this->EE->db->get('sites');" to line 183 of above-mentioned file

/**
 * 2. Tell the script about both our parent and child databases.
 */

$child_site_id 						= 2;
$child_site_name					= 'dpr_review';
$parent_last_channel_id 			= 25; 	// SELECT MAX(channel_id) FROM exp_channels
$parent_last_entry_id 				= 984; 	// SELECT MAX(entry_id) FROM exp_channel_data
$parent_last_playa_rel_id 			= 6299; // SELECT MAX(rel_id) FROM exp_playa_relationships
$parent_last_field_id 				= 67; 	// SELECT MAX(field_id) FROM exp_channel_fields
$parent_last_matrix_col_id 			= 10; 	// SELECT MAX(col_id) FROM exp_matrix_cols
$parent_last_matrix_row_id 			= 54; 	// SELECT MAX(row_id) FROM exp_matrix_data
$parent_last_category_group_id 		= 2; 	// SELECT MAX(group_id) FROM exp_category_groups
$parent_last_category_id 			= 22; 	// SELECT MAX(cat_id) FROM exp_categories
$parent_last_field_group_id			= 17; 	// SELECT MAX(group_id) FROM exp_field_groups
$parent_last_field_formatting_id	= 201; 	// SELECT MAX(formatting_id) FROM exp_field_formatting
$parent_last_template_group_id		= 7; 	// SELECT MAX(group_id) FROM exp_template_groups
$parent_last_template_id			= 455; 	// SELECT MAX(template_id) FROM exp_templates
$parent_last_specialty_template_id	= 32; 	// SELECT MAX(template_id) FROM exp_specialty_templates
$parent_last_upload_pref_id			= 8; 	// SELECT MAX(id) FROM exp_upload_prefs
$sql_output_path					= '/Users/matt/Desktop/';
$author_map 						= array(
										1 => 1,
										2 => 2,
										3 => 3
									); // child site member_id => parent site member_id

/**
 * 3. Paste this entire document into a template, enable PHP (input or output; doesn't matter), and load the URL in your browser ONCE.
 */
 
/**
 * 4. Test this EE site (public+admin) and make sure everything's working sufficiently.
 */
 
/**
 * 5. Create the new site from within the parent EE control panel. Be sure to use the same site_name!
 */
 
/**
 * 6. Copy and run outputted SQL from step 3 on the parent EE database.
 */
 
/**
 * 7. Import the resulting SQL dump into the parent EE database, ignoring errors.
 */
	
/**
 * 8. If you used EE's native file management, re-sychronize your upload locations.
 */
	
/**
 * 9. See what went wrong, then improve this script.
 */
	
?><!DOCTYPE html>
<html>
<head>
	<title>Update Database</title>
	<style>
	body {
		font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
		font-size: 0.8em;
		line-height: 1.2em;
	}
	</style>
</head>
<body>
	
	<h1>Multiple Site Manager Merge Script</h1>
<?php

global $log;
global $tables;

$prefs = $this->EE->db->from('exp_sites')->select('site_description,site_system_preferences,site_template_preferences,site_channel_preferences,site_bootstrap_checksums')->get()->row();

echo '<h3>site_system_preferences</h3>';
print_r(array_merge(unserialize(base64_decode($prefs->site_system_preferences))));
echo '<h3>site_template_preferences</h3>';
print_r(array_merge(unserialize(base64_decode($prefs->site_template_preferences))));
echo '<h3>site_channel_preferences</h3>';
print_r(array_merge(unserialize(base64_decode($prefs->site_channel_preferences))));


// make some buckets
$channel_ids 				= array(); // index of original and modified channel_ids
$entry_ids 					= array(); // index of original and modified entry_ids
$template_ids 				= array(); // index of original and modified template_ids
$channel_field_ids 			= array(); // index of original and modified field_ids
$matrix_col_ids				= array(); // index of original and modified Matrix col_ids
$log						= array(); // log work done for output

// load stuff
$this->EE->load->dbforge();
$this->EE->load->dbutil();

$tables = $this->EE->db->list_tables();

// TODO: update any channel_id's contained in hashes
// TODO: update any entry_id's contained in hashes

/**
 * Update and replace things.
 */

// update site_id values
global_replace(array(1 => $child_site_id), array('site_id'));
$this->EE->db->update('exp_sites', array('site_name' => $child_site_name));

// update channel_id values
$channel_ids = shift_index_ids('exp_channels', 'channel_id', $parent_last_channel_id);
global_replace($channel_ids, array('channel_id'));

// update channel entry_id values
$entry_ids = shift_index_ids('exp_channel_data', 'entry_id', $parent_last_entry_id);
global_replace($entry_ids, array('entry_id', 'parent_entry_id', 'child_entry_id'));

// update Playa's references to channels in exp_channel_fields (they're stuffed in field_settings)
$fields = $this->EE->db->from('exp_channel_fields')->where('field_type', 'playa')->get();

foreach($fields->result() as $field)
{
	$field_settings = array_merge(unserialize(base64_decode($field->field_settings)));
	
	foreach($field_settings as $setting_name => $setting_value)
	{
		if ($setting_name == 'channels')
		{
			$channel_settings = array();
			
			foreach($channel_ids as $old => $new)
			{
				if (in_array($old, $setting_value)) $channel_settings[] = $new;
			}
			
			if (count($channel_settings) > 0) $field_settings[$setting_name] = $channel_settings;
		}
	}
	
	$data = array('field_settings' => base64_encode(serialize($field_settings)));
	
	$this->EE->db->where('field_id', $field->field_id)->update('exp_channel_fields', $data); // update the row in exp_channel_fields
	
	$log[] = 'Updated Playa field settings for "'.$field->field_name.'" field.';
}

// shift Playa's rel_id values
$play_rel_ids = shift_index_ids('exp_playa_relationships', 'rel_id', $parent_last_playa_rel_id);

// shift channel field_id values
$channel_field_ids = shift_index_ids('exp_channel_fields', 'field_id', $parent_last_field_id);

// update the field_id values now that they've changed
global_replace($channel_field_ids, array('field_id', 'parent_field_id'));

// update column names in exp_channel_data so they match then new field_id values
$insert_sql = '';

foreach($channel_field_ids as $old => $new)
{
	$this->EE->dbforge->modify_column('channel_data', array('field_id_'.$old => array('name' => 'field_id_'.$new, 'type' => 'TEXT')));
	$this->EE->dbforge->modify_column('channel_data', array('field_ft_'.$old => array('name' => 'field_ft_'.$new, 'type' => 'TINYTEXT')));
	
	$insert_sql .= "ALTER TABLE `exp_channel_data` ADD field_id_".$new." TEXT; \n";
	$insert_sql .= "ALTER TABLE `exp_channel_data` ADD field_ft_".$new." TINYTEXT; \n";
}

// output SQL for insert columns into new database
$log[] = $insert_sql;

// shift Matrix's col_id values
$matrix_col_ids = shift_index_ids('exp_matrix_cols', 'col_id', $parent_last_matrix_col_id);

// update column names in exp_matrix_data so they match then new col_id values
$insert_sql = '';

foreach($matrix_col_ids as $old => $new)
{
	$this->EE->dbforge->modify_column('matrix_data', array('col_id_'.$old => array('name' => 'col_id_'.$new, 'type' => 'TEXT')));
	$this->EE->db->where('parent_col_id', $old)->update('exp_playa_relationships', array('parent_col_id' => $new));

	$insert_sql .= "ALTER TABLE `exp_matrix_data` ADD col_id_".$new." TEXT; \n";
}

// output SQL for insert columns into new database
$log[] = $insert_sql;

// update Playa's references to channel_id values, hashed in exp_matrix_cols.col_settings 
$cols = $this->EE->db->from('exp_matrix_cols')->get();

foreach($cols->result() as $col)
{
	$col_settings = array_merge(unserialize(base64_decode($col->col_settings)));
	
	foreach($col_settings as $setting_name => $setting_value)
	{
		if ($setting_name == 'channels')
		{
			$channel_settings = array();
			
			foreach($channel_ids as $old => $new)
			{
				if (in_array($old, $setting_value)) $channel_settings[] = $new;
			}
			
			if (count($channel_settings) > 0) $col_settings[$setting_name] = $channel_settings;
		}
	}
	
	$data = array('col_settings' => base64_encode(serialize($col_settings)));
	
	$this->EE->db->where('col_id', $col->col_id)->update('exp_matrix_cols', $data); // update the row in exp_matrix_cols
	
	$log[] = 'Updated Matrix channel_id settings for "'.$col->col_name.'" column.';
}
 
// shift Matrix's row_id values
$matrix_row_ids = shift_index_ids('exp_matrix_data', 'row_id', $parent_last_matrix_row_id);

// update Playa's parent_row_id's to match new Matrix values
foreach($matrix_row_ids as $old => $new)
{
	$this->EE->db->where('parent_row_id', $old)->update('exp_playa_relationships', array('parent_row_id' => $new));
}

// shift category group_id values
$category_group_ids = shift_index_ids('exp_category_groups', 'group_id', $parent_last_category_group_id);

foreach($category_group_ids as $old => $new)
{
	$this->EE->db->where('group_id', $old)->update('exp_categories', array('group_id' => $new));
	$this->EE->db->where('cat_group', $old)->update('exp_channels', array('cat_group' => $new));
	// TODO: address category parents (wasn't applicable for me)
}

// shift category_id values
$category_ids = shift_index_ids('exp_categories', 'cat_id', $parent_last_category_id);
global_replace($category_ids, array('cat_id'));

// shift field group ID values
$field_group_ids = shift_index_ids('exp_field_groups', 'group_id', $parent_last_field_group_id);

foreach($field_group_ids as $old => $new)
{
	$this->EE->db->where('field_group', $old)->update('exp_channels', array('field_group' => $new));
	$this->EE->db->where('group_id', $old)->update('exp_channel_fields', array('group_id' => $new));
}

// shift field formatting ID values
$field_group_ids = shift_index_ids('exp_field_formatting', 'formatting_id', $parent_last_field_formatting_id);

// update field formatting ID's
// IMPORTANT: the formatting type must already exist in the parent EE install, this simply maps the old ID to a new one

$field_type_map = array(
						10 => 11, // matrix
						11 => 12 // playa
					); // where is fieldtype_id referred to?

// shift template group ID values
$template_group_ids = shift_index_ids('exp_template_groups', 'group_id', $parent_last_template_group_id);

foreach($template_group_ids as $old => $new)
{
	$this->EE->db->where('group_id', $old)->update('exp_templates', array('group_id' => $new));
}

// shift template_id values
$template_ids = shift_index_ids('exp_templates', 'template_id', $parent_last_template_id);

// shift specialty template_id values
$specialty_template_ids = shift_index_ids('exp_specialty_templates', 'template_id', $parent_last_specialty_template_id);

// update upload locations
$upload_pref_ids = shift_index_ids('exp_upload_prefs', 'id', $parent_last_upload_pref_id);

// TODO: support comments
// TODO: support statuses
// TODO: update exp_fieldtypes (use map from old site to new one)
// TODO: update authors (wasn't applicable for me)
// TODO: figure out how to address addons

/**
 * Delete unnecessary tables.
 */
  
$drop_tables = array(
 	"exp_accessories",
 	"exp_captcha",
 	"exp_category_fields",
 	"exp_channel_entries_autosave",
 	"exp_channel_member_groups",
 	"exp_cp_log",
 	"exp_cp_search_index",
 	"exp_email_cache",
 	"exp_email_cache_mg",
 	"exp_email_cache_ml",
 	"exp_email_console_cache",
 	"exp_email_tracker",
 	"exp_entry_ping_status",
 	"exp_global_variables",
 	"exp_html_buttons",
 	"exp_layout_publish",
 	"exp_member_bulletin_board",
 	"exp_member_data",
 	"exp_member_fields",
 	"exp_member_groups",
 	"exp_member_homepage",
 	"exp_member_search",
 	"exp_members",
 	"exp_message_attachments",
 	"exp_message_copies",
 	"exp_message_data",
 	"exp_message_folders",
 	"exp_message_listed",
 	"exp_module_member_groups",
 	"exp_online_users",
 	"exp_password_lockout",
 	"exp_ping_servers",
 	"exp_relationships",
 	"exp_reset_password",
 	"exp_revision_tracker",
 	"exp_search",
 	"exp_search_log",
 	"exp_security_hashes",
 	"exp_sessions",
 	"exp_snippets",
 	"exp_stats",
 	"exp_status_no_access",
 	"exp_template_member_groups",
 	"exp_template_no_access",
 	"exp_throttle",
 	"exp_upload_no_access"
);
 
/**
 * Export SQL.
 */

$prefs = array(
                'ignore'      => $drop_tables,
                'format'      => 'txt',
                'filename'    => 'dpr-review.dev.com.TEST.sql',
                'add_drop'    => FALSE,              // Whether to add DROP TABLE statements to backup file
                'add_insert'  => TRUE,              // Whether to add INSERT data to backup file
                'newline'     => "\n"               // Newline character used in backup file
              );

$backup =& $this->EE->dbutil->backup($prefs); 

/**
 * Run additional processing to SQL dump before it's written.
 */

$search = array();
$replace = array();

foreach($upload_pref_ids as $old => $new)
{
	$search[] = '{filedir_'.$old.'}';
	$replace[] = '{filedir_'.$new.'}';
}

$modified_backup = str_replace($search, $replace, $backup);

$log[] = 'searched SQL for: '.print_r($search, true);
$log[] = 'replaced SQL with: '.print_r($replace, true);

// $this->EE->load->helper('download');
// force_download($prefs['filename'], $backup);

/**
 * Write SQL.
 */

$this->EE->load->helper('file');
write_file($sql_output_path.$prefs['filename'], $modified_backup); 

/**
 * Print log.
 */
 
echo '<ul>';

foreach($log as $line)
{
	echo '<li>'.$line.'</li>';
}

echo '</ul>';






/**
 * Function for shifting index ID's within a table.
 */

function shift_index_ids($table_name, $field_name, $increment)
{
	global $log;
	$EE = get_instance();
	
	$min_id = $EE->db->from($table_name)->select_min($field_name)->get()->row()->{$field_name};

	if ($min_id <= $increment)
	{
		$id_map 		= array();
		$update_count 	= 0;
		$items 			= $EE->db->order_by($field_name, 'desc')->get($table_name);
 
		foreach($items->result() as $item)
		{
			$old_id 			= $item->{$field_name};
			$new_id 			= $old_id + $increment;
			$id_map[$old_id]	= $new_id;
	
			$EE->db->where($field_name, $item->{$field_name})->update($table_name, array($field_name => $new_id));
		
			$update_count++;
		}
	
		$log[] = 'Updated '.$update_count.' '.$table_name.'.'.$field_name.' values.';
		return $id_map;
	}
	else
	{
		$log[] = 'Skipping '.$table_name.'.'.$field_name.' update since lowest ID ('.$min_id.') was less than starting point ('.$increment.').';
		return FALSE;
	}
}

/**
 * Function for updating IDs throughout the database.
 */

function global_replace($id_map, $fields)
{
	global $tables;
	$EE = get_instance();
	
	foreach($tables as $table_name)
	{	
		foreach($fields as $field_name)
		{
			if ($EE->db->field_exists($field_name, $table_name))
			{
				foreach($id_map as $old => $new)
				{
					$EE->db->where($field_name, $old)->update($table_name, array($field_name => $new));
				}
			}
		}
	}
}

?>
</body>
</html>